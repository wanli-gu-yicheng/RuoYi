package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EduExamMapper;
import com.ruoyi.system.domain.EduExam;
import com.ruoyi.system.service.IEduExamService;
import com.ruoyi.common.core.text.Convert;

/**
 * 考试成绩Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-07
 */
@Service
public class EduExamServiceImpl implements IEduExamService 
{
    @Autowired
    private EduExamMapper eduExamMapper;

    /**
     * 查询考试成绩
     * 
     * @param examId 考试成绩主键
     * @return 考试成绩
     */
    @Override
    public EduExam selectEduExamByExamId(Long examId)
    {
        return eduExamMapper.selectEduExamByExamId(examId);
    }

    /**
     * 查询考试成绩列表
     * 
     * @param eduExam 考试成绩
     * @return 考试成绩
     */
    @Override
    public List<EduExam> selectEduExamList(EduExam eduExam)
    {
        return eduExamMapper.selectEduExamList(eduExam);
    }

    /**
     * 新增考试成绩
     * 
     * @param eduExam 考试成绩
     * @return 结果
     */
    @Override
    public int insertEduExam(EduExam eduExam)
    {
        return eduExamMapper.insertEduExam(eduExam);
    }

    /**
     * 修改考试成绩
     * 
     * @param eduExam 考试成绩
     * @return 结果
     */
    @Override
    public int updateEduExam(EduExam eduExam)
    {
        return eduExamMapper.updateEduExam(eduExam);
    }

    /**
     * 批量删除考试成绩
     * 
     * @param examIds 需要删除的考试成绩主键
     * @return 结果
     */
    @Override
    public int deleteEduExamByExamIds(String examIds)
    {
        return eduExamMapper.deleteEduExamByExamIds(Convert.toStrArray(examIds));
    }

    /**
     * 删除考试成绩信息
     * 
     * @param examId 考试成绩主键
     * @return 结果
     */
    @Override
    public int deleteEduExamByExamId(Long examId)
    {
        return eduExamMapper.deleteEduExamByExamId(examId);
    }
}
