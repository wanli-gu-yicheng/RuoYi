package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.EduRegin;

/**
 * 地区表Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public interface EduReginMapper 
{
    /**
     * 查询地区表
     * 
     * @param reginId 地区表主键
     * @return 地区表
     */
    public EduRegin selectEduReginByReginId(Long reginId);

    /**
     * 查询地区表列表
     * 
     * @param eduRegin 地区表
     * @return 地区表集合
     */
    public List<EduRegin> selectEduReginList(EduRegin eduRegin);

    /**
     * 新增地区表
     * 
     * @param eduRegin 地区表
     * @return 结果
     */
    public int insertEduRegin(EduRegin eduRegin);

    /**
     * 修改地区表
     * 
     * @param eduRegin 地区表
     * @return 结果
     */
    public int updateEduRegin(EduRegin eduRegin);

    /**
     * 删除地区表
     * 
     * @param reginId 地区表主键
     * @return 结果
     */
    public int deleteEduReginByReginId(Long reginId);

    /**
     * 批量删除地区表
     * 
     * @param reginIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEduReginByReginIds(String[] reginIds);
}
