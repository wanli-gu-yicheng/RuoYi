package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.EduRegin;
import com.ruoyi.system.service.IEduReginService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 地区表Controller
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Controller
@RequestMapping("/system/regin")
public class EduReginController extends BaseController
{
    private String prefix = "system/regin";

    @Autowired
    private IEduReginService eduReginService;

    @RequiresPermissions("system:regin:view")
    @GetMapping()
    public String regin()
    {
        return prefix + "/regin";
    }

    /**
     * 查询地区表树列表
     */
    @RequiresPermissions("system:regin:list")
    @PostMapping("/list")
    @ResponseBody
    public List<EduRegin> list(EduRegin eduRegin)
    {
        List<EduRegin> list = eduReginService.selectEduReginList(eduRegin);
        return list;
    }

    /**
     * 导出地区表列表
     */
    @RequiresPermissions("system:regin:export")
    @Log(title = "地区表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(EduRegin eduRegin)
    {
        List<EduRegin> list = eduReginService.selectEduReginList(eduRegin);
        ExcelUtil<EduRegin> util = new ExcelUtil<EduRegin>(EduRegin.class);
        return util.exportExcel(list, "地区表数据");
    }

    /**
     * 新增地区表
     */
    @GetMapping(value = { "/add/{reginId}", "/add/" })
    public String add(@PathVariable(value = "reginId", required = false) Long reginId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(reginId))
        {
            mmap.put("eduRegin", eduReginService.selectEduReginByReginId(reginId));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存地区表
     */
    @RequiresPermissions("system:regin:add")
    @Log(title = "地区表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(EduRegin eduRegin)
    {
        return toAjax(eduReginService.insertEduRegin(eduRegin));
    }

    /**
     * 修改地区表
     */
    @RequiresPermissions("system:regin:edit")
    @GetMapping("/edit/{reginId}")
    public String edit(@PathVariable("reginId") Long reginId, ModelMap mmap)
    {
        EduRegin eduRegin = eduReginService.selectEduReginByReginId(reginId);
        mmap.put("eduRegin", eduRegin);
        return prefix + "/edit";
    }

    /**
     * 修改保存地区表
     */
    @RequiresPermissions("system:regin:edit")
    @Log(title = "地区表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(EduRegin eduRegin)
    {
        return toAjax(eduReginService.updateEduRegin(eduRegin));
    }

    /**
     * 删除
     */
    @RequiresPermissions("system:regin:remove")
    @Log(title = "地区表", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{reginId}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("reginId") Long reginId)
    {
        return toAjax(eduReginService.deleteEduReginByReginId(reginId));
    }

    /**
     * 选择地区表树
     */
    @GetMapping(value = { "/selectReginTree/{reginId}", "/selectReginTree/" })
    public String selectReginTree(@PathVariable(value = "reginId", required = false) Long reginId, ModelMap mmap)
    {
        if (StringUtils.isNotNull(reginId))
        {
            mmap.put("eduRegin", eduReginService.selectEduReginByReginId(reginId));
        }
        return prefix + "/tree";
    }

    /**
     * 加载地区表树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = eduReginService.selectEduReginTree();
        return ztrees;
    }
}
