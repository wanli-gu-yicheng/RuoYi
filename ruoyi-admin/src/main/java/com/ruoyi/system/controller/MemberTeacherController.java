package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MemberTeacher;
import com.ruoyi.system.service.IMemberTeacherService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 教师Controller
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Controller
@RequestMapping("/system/teacher")
public class MemberTeacherController extends BaseController
{
    private String prefix = "system/teacher";

    @Autowired
    private IMemberTeacherService memberTeacherService;

    @RequiresPermissions("system:teacher:view")
    @GetMapping()
    public String teacher()
    {
        return prefix + "/teacher";
    }

    /**
     * 查询教师列表
     */
    @RequiresPermissions("system:teacher:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberTeacher memberTeacher)
    {
        startPage();
        List<MemberTeacher> list = memberTeacherService.selectMemberTeacherList(memberTeacher);
        return getDataTable(list);
    }

    /**
     * 导出教师列表
     */
    @RequiresPermissions("system:teacher:export")
    @Log(title = "教师", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberTeacher memberTeacher)
    {
        List<MemberTeacher> list = memberTeacherService.selectMemberTeacherList(memberTeacher);
        ExcelUtil<MemberTeacher> util = new ExcelUtil<MemberTeacher>(MemberTeacher.class);
        return util.exportExcel(list, "教师数据");
    }

    /**
     * 新增教师
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存教师
     */
    @RequiresPermissions("system:teacher:add")
    @Log(title = "教师", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberTeacher memberTeacher)
    {
        return toAjax(memberTeacherService.insertMemberTeacher(memberTeacher));
    }

    /**
     * 修改教师
     */
    @RequiresPermissions("system:teacher:edit")
    @GetMapping("/edit/{teacherId}")
    public String edit(@PathVariable("teacherId") Long teacherId, ModelMap mmap)
    {
        MemberTeacher memberTeacher = memberTeacherService.selectMemberTeacherByTeacherId(teacherId);
        mmap.put("memberTeacher", memberTeacher);
        return prefix + "/edit";
    }

    /**
     * 修改保存教师
     */
    @RequiresPermissions("system:teacher:edit")
    @Log(title = "教师", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberTeacher memberTeacher)
    {
        return toAjax(memberTeacherService.updateMemberTeacher(memberTeacher));
    }

    /**
     * 删除教师
     */
    @RequiresPermissions("system:teacher:remove")
    @Log(title = "教师", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberTeacherService.deleteMemberTeacherByTeacherIds(ids));
    }
}
