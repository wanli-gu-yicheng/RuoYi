package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EduExperienceMapper;
import com.ruoyi.system.domain.EduExperience;
import com.ruoyi.system.service.IEduExperienceService;
import com.ruoyi.common.core.text.Convert;

/**
 * 工作经历Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-17
 */
@Service
public class EduExperienceServiceImpl implements IEduExperienceService 
{
    @Autowired
    private EduExperienceMapper eduExperienceMapper;

    /**
     * 查询工作经历
     * 
     * @param teacherCode 工作经历主键
     * @return 工作经历
     */
    @Override
    public EduExperience selectEduExperienceByTeacherCode(String teacherCode)
    {
        return eduExperienceMapper.selectEduExperienceByTeacherCode(teacherCode);
    }

    /**
     * 查询工作经历列表
     * 
     * @param eduExperience 工作经历
     * @return 工作经历
     */
    @Override
    public List<EduExperience> selectEduExperienceList(EduExperience eduExperience)
    {
        return eduExperienceMapper.selectEduExperienceList(eduExperience);
    }

    /**
     * 新增工作经历
     * 
     * @param eduExperience 工作经历
     * @return 结果
     */
    @Override
    public int insertEduExperience(EduExperience eduExperience)
    {
        return eduExperienceMapper.insertEduExperience(eduExperience);
    }

    /**
     * 修改工作经历
     * 
     * @param eduExperience 工作经历
     * @return 结果
     */
    @Override
    public int updateEduExperience(EduExperience eduExperience)
    {
        return eduExperienceMapper.updateEduExperience(eduExperience);
    }

    /**
     * 批量删除工作经历
     * 
     * @param teacherCodes 需要删除的工作经历主键
     * @return 结果
     */
    @Override
    public int deleteEduExperienceByTeacherCodes(String teacherCodes)
    {
        return eduExperienceMapper.deleteEduExperienceByTeacherCodes(Convert.toStrArray(teacherCodes));
    }

    /**
     * 删除工作经历信息
     * 
     * @param teacherCode 工作经历主键
     * @return 结果
     */
    @Override
    public int deleteEduExperienceByTeacherCode(String teacherCode)
    {
        return eduExperienceMapper.deleteEduExperienceByTeacherCode(teacherCode);
    }
}
