package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EcuMemberMapper;
import com.ruoyi.system.domain.EcuMember;
import com.ruoyi.system.service.IEcuMemberService;
import com.ruoyi.common.core.text.Convert;

/**
 * 会员表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Service
public class EcuMemberServiceImpl implements IEcuMemberService 
{
    @Autowired
    private EcuMemberMapper ecuMemberMapper;

    /**
     * 查询会员表
     * 
     * @param memberId 会员表主键
     * @return 会员表
     */
    @Override
    public EcuMember selectEcuMemberByMemberId(Long memberId)
    {
        return ecuMemberMapper.selectEcuMemberByMemberId(memberId);
    }

    /**
     * 查询会员表列表
     * 
     * @param ecuMember 会员表
     * @return 会员表
     */
    @Override
    public List<EcuMember> selectEcuMemberList(EcuMember ecuMember)
    {
        return ecuMemberMapper.selectEcuMemberList(ecuMember);
    }

    /**
     * 新增会员表
     * 
     * @param ecuMember 会员表
     * @return 结果
     */
    @Override
    public int insertEcuMember(EcuMember ecuMember)
    {
        ecuMember.setCreateTime(DateUtils.getNowDate());
        return ecuMemberMapper.insertEcuMember(ecuMember);
    }

    /**
     * 修改会员表
     * 
     * @param ecuMember 会员表
     * @return 结果
     */
    @Override
    public int updateEcuMember(EcuMember ecuMember)
    {
        ecuMember.setUpdateTime(DateUtils.getNowDate());
        return ecuMemberMapper.updateEcuMember(ecuMember);
    }

    /**
     * 批量删除会员表
     * 
     * @param memberIds 需要删除的会员表主键
     * @return 结果
     */
    @Override
    public int deleteEcuMemberByMemberIds(String memberIds)
    {
        return ecuMemberMapper.deleteEcuMemberByMemberIds(Convert.toStrArray(memberIds));
    }

    /**
     * 删除会员表信息
     * 
     * @param memberId 会员表主键
     * @return 结果
     */
    @Override
    public int deleteEcuMemberByMemberId(Long memberId)
    {
        return ecuMemberMapper.deleteEcuMemberByMemberId(memberId);
    }
}
