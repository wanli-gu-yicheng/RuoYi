package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.EduExam;

/**
 * 考试成绩Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-07
 */
public interface EduExamMapper 
{
    /**
     * 查询考试成绩
     * 
     * @param examId 考试成绩主键
     * @return 考试成绩
     */
    public EduExam selectEduExamByExamId(Long examId);

    /**
     * 查询考试成绩列表
     * 
     * @param eduExam 考试成绩
     * @return 考试成绩集合
     */
    public List<EduExam> selectEduExamList(EduExam eduExam);

    /**
     * 新增考试成绩
     * 
     * @param eduExam 考试成绩
     * @return 结果
     */
    public int insertEduExam(EduExam eduExam);

    /**
     * 修改考试成绩
     * 
     * @param eduExam 考试成绩
     * @return 结果
     */
    public int updateEduExam(EduExam eduExam);

    /**
     * 删除考试成绩
     * 
     * @param examId 考试成绩主键
     * @return 结果
     */
    public int deleteEduExamByExamId(Long examId);

    /**
     * 批量删除考试成绩
     * 
     * @param examIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEduExamByExamIds(String[] examIds);
}
