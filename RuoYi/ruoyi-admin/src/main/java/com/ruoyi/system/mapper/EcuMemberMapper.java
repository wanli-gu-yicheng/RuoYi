package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.EcuMember;

/**
 * 会员表Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public interface EcuMemberMapper 
{
    /**
     * 查询会员表
     * 
     * @param memberId 会员表主键
     * @return 会员表
     */
    public EcuMember selectEcuMemberByMemberId(Long memberId);

    /**
     * 查询会员表列表
     * 
     * @param ecuMember 会员表
     * @return 会员表集合
     */
    public List<EcuMember> selectEcuMemberList(EcuMember ecuMember);

    /**
     * 新增会员表
     * 
     * @param ecuMember 会员表
     * @return 结果
     */
    public int insertEcuMember(EcuMember ecuMember);

    /**
     * 修改会员表
     * 
     * @param ecuMember 会员表
     * @return 结果
     */
    public int updateEcuMember(EcuMember ecuMember);

    /**
     * 删除会员表
     * 
     * @param memberId 会员表主键
     * @return 结果
     */
    public int deleteEcuMemberByMemberId(Long memberId);

    /**
     * 批量删除会员表
     * 
     * @param memberIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEcuMemberByMemberIds(String[] memberIds);
}
