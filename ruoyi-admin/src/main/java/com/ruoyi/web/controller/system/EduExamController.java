package com.ruoyi.web.controller.system;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.EduExam;
import com.ruoyi.system.service.IEduExamService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 考试成绩Controller
 * 
 * @author ruoyi
 * @date 2024-04-07
 */
@Controller
@RequestMapping("/system/exam")
public class EduExamController extends BaseController
{
    private String prefix = "system/exam";

    @Autowired
    private IEduExamService eduExamService;

    @RequiresPermissions("system:exam:view")
    @GetMapping()
    public String exam()
    {
        return prefix + "/exam";
    }

    /**
     * 查询考试成绩列表
     */
    @RequiresPermissions("system:exam:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(EduExam eduExam)
    {
        startPage();
        List<EduExam> list = eduExamService.selectEduExamList(eduExam);
        return getDataTable(list);
    }

    /**
     * 导出考试成绩列表
     */
    @RequiresPermissions("system:exam:export")
    @Log(title = "考试成绩", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(EduExam eduExam)
    {
        List<EduExam> list = eduExamService.selectEduExamList(eduExam);
        ExcelUtil<EduExam> util = new ExcelUtil<EduExam>(EduExam.class);
        return util.exportExcel(list, "考试成绩数据");
    }

    /**
     * 新增考试成绩
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存考试成绩
     */
    @RequiresPermissions("system:exam:add")
    @Log(title = "考试成绩", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(EduExam eduExam)
    {
        return toAjax(eduExamService.insertEduExam(eduExam));
    }

    /**
     * 修改考试成绩
     */
    @RequiresPermissions("system:exam:edit")
    @GetMapping("/edit/{examId}")
    public String edit(@PathVariable("examId") Long examId, ModelMap mmap)
    {
        EduExam eduExam = eduExamService.selectEduExamByExamId(examId);
        mmap.put("eduExam", eduExam);
        return prefix + "/edit";
    }

    /**
     * 修改保存考试成绩
     */
    @RequiresPermissions("system:exam:edit")
    @Log(title = "考试成绩", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(EduExam eduExam)
    {
        return toAjax(eduExamService.updateEduExam(eduExam));
    }

    /**
     * 删除考试成绩
     */
    @RequiresPermissions("system:exam:remove")
    @Log(title = "考试成绩", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(eduExamService.deleteEduExamByExamIds(ids));
    }
}
