package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EduKlassMapper;
import com.ruoyi.system.domain.EduKlass;
import com.ruoyi.system.service.IEduKlassService;
import com.ruoyi.common.core.text.Convert;

/**
 * 班级Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-03-31
 */
@Service
public class EduKlassServiceImpl implements IEduKlassService 
{
    @Autowired
    private EduKlassMapper eduKlassMapper;

    /**
     * 查询班级
     * 
     * @param klassId 班级主键
     * @return 班级
     */
    @Override
    public EduKlass selectEduKlassByKlassId(Long klassId)
    {
        return eduKlassMapper.selectEduKlassByKlassId(klassId);
    }

    /**
     * 查询班级列表
     * 
     * @param eduKlass 班级
     * @return 班级
     */
    @Override
    public List<EduKlass> selectEduKlassList(EduKlass eduKlass)
    {
        return eduKlassMapper.selectEduKlassList(eduKlass);
    }

    /**
     * 新增班级
     * 
     * @param eduKlass 班级
     * @return 结果
     */
    @Override
    public int insertEduKlass(EduKlass eduKlass)
    {
        return eduKlassMapper.insertEduKlass(eduKlass);
    }

    /**
     * 修改班级
     * 
     * @param eduKlass 班级
     * @return 结果
     */
    @Override
    public int updateEduKlass(EduKlass eduKlass)
    {
        return eduKlassMapper.updateEduKlass(eduKlass);
    }

    /**
     * 批量删除班级
     * 
     * @param klassIds 需要删除的班级主键
     * @return 结果
     */
    @Override
    public int deleteEduKlassByKlassIds(String klassIds)
    {
        return eduKlassMapper.deleteEduKlassByKlassIds(Convert.toStrArray(klassIds));
    }

    /**
     * 删除班级信息
     * 
     * @param klassId 班级主键
     * @return 结果
     */
    @Override
    public int deleteEduKlassByKlassId(Long klassId)
    {
        return eduKlassMapper.deleteEduKlassByKlassId(klassId);
    }

    @Override
    public List<EduKlass> selectEduKlassAll() {
        return eduKlassMapper.selectEduKlassAll();
    }
}


