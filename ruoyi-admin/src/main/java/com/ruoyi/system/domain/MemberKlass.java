package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 班级表对象 member_klass
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public class MemberKlass extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 班级ID */
    private Long klassId;

    /** 班级名称 */
    @Excel(name = "班级名称")
    private String klassName;

    /** 教师 */
    @Excel(name = "教师")
    private Long teacherId;

    public void setKlassId(Long klassId) 
    {
        this.klassId = klassId;
    }

    public Long getKlassId() 
    {
        return klassId;
    }
    public void setKlassName(String klassName) 
    {
        this.klassName = klassName;
    }

    public String getKlassName() 
    {
        return klassName;
    }
    public void setTeacherId(Long teacherId) 
    {
        this.teacherId = teacherId;
    }

    public Long getTeacherId() 
    {
        return teacherId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("klassId", getKlassId())
            .append("klassName", getKlassName())
            .append("teacherId", getTeacherId())
            .toString();
    }
}
