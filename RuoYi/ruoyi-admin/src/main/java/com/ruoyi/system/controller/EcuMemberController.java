package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.EcuMember;
import com.ruoyi.system.service.IEcuMemberService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员表Controller
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Controller
@RequestMapping("/system/member")
public class EcuMemberController extends BaseController
{
    private String prefix = "system/member";

    @Autowired
    private IEcuMemberService ecuMemberService;

    @RequiresPermissions("system:member:view")
    @GetMapping()
    public String member()
    {
        return prefix + "/member";
    }

    /**
     * 查询会员表列表
     */
    @RequiresPermissions("system:member:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(EcuMember ecuMember)
    {
        startPage();
        List<EcuMember> list = ecuMemberService.selectEcuMemberList(ecuMember);
        return getDataTable(list);
    }

    /**
     * 导出会员表列表
     */
    @RequiresPermissions("system:member:export")
    @Log(title = "会员表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(EcuMember ecuMember)
    {
        List<EcuMember> list = ecuMemberService.selectEcuMemberList(ecuMember);
        ExcelUtil<EcuMember> util = new ExcelUtil<EcuMember>(EcuMember.class);
        return util.exportExcel(list, "会员表数据");
    }

    /**
     * 新增会员表
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存会员表
     */
    @RequiresPermissions("system:member:add")
    @Log(title = "会员表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(EcuMember ecuMember)
    {
        return toAjax(ecuMemberService.insertEcuMember(ecuMember));
    }

    /**
     * 修改会员表
     */
    @RequiresPermissions("system:member:edit")
    @GetMapping("/edit/{memberId}")
    public String edit(@PathVariable("memberId") Long memberId, ModelMap mmap)
    {
        EcuMember ecuMember = ecuMemberService.selectEcuMemberByMemberId(memberId);
        mmap.put("ecuMember", ecuMember);
        return prefix + "/edit";
    }

    /**
     * 修改保存会员表
     */
    @RequiresPermissions("system:member:edit")
    @Log(title = "会员表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(EcuMember ecuMember)
    {
        return toAjax(ecuMemberService.updateEcuMember(ecuMember));
    }

    /**
     * 删除会员表
     */
    @RequiresPermissions("system:member:remove")
    @Log(title = "会员表", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ecuMemberService.deleteEcuMemberByMemberIds(ids));
    }
}
