package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MemberKlassMapper;
import com.ruoyi.system.domain.MemberKlass;
import com.ruoyi.system.service.IMemberKlassService;
import com.ruoyi.common.core.text.Convert;

/**
 * 班级表Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Service
public class MemberKlassServiceImpl implements IMemberKlassService 
{
    @Autowired
    private MemberKlassMapper memberKlassMapper;

    /**
     * 查询班级表
     * 
     * @param klassId 班级表主键
     * @return 班级表
     */
    @Override
    public MemberKlass selectMemberKlassByKlassId(Long klassId)
    {
        return memberKlassMapper.selectMemberKlassByKlassId(klassId);
    }

    /**
     * 查询班级表列表
     * 
     * @param memberKlass 班级表
     * @return 班级表
     */
    @Override
    public List<MemberKlass> selectMemberKlassList(MemberKlass memberKlass)
    {
        return memberKlassMapper.selectMemberKlassList(memberKlass);
    }

    /**
     * 新增班级表
     * 
     * @param memberKlass 班级表
     * @return 结果
     */
    @Override
    public int insertMemberKlass(MemberKlass memberKlass)
    {
        return memberKlassMapper.insertMemberKlass(memberKlass);
    }

    /**
     * 修改班级表
     * 
     * @param memberKlass 班级表
     * @return 结果
     */
    @Override
    public int updateMemberKlass(MemberKlass memberKlass)
    {
        return memberKlassMapper.updateMemberKlass(memberKlass);
    }

    /**
     * 批量删除班级表
     * 
     * @param klassIds 需要删除的班级表主键
     * @return 结果
     */
    @Override
    public int deleteMemberKlassByKlassIds(String klassIds)
    {
        return memberKlassMapper.deleteMemberKlassByKlassIds(Convert.toStrArray(klassIds));
    }

    /**
     * 删除班级表信息
     * 
     * @param klassId 班级表主键
     * @return 结果
     */
    @Override
    public int deleteMemberKlassByKlassId(Long klassId)
    {
        return memberKlassMapper.deleteMemberKlassByKlassId(klassId);
    }
}
