package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 工作经历对象 edu_experience
 * 
 * @author ruoyi
 * @date 2024-03-17
 */
public class EduExperience extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 教师工号 */
    @Excel(name = "教师工号")
    private String teacherCode;

    /** 毕业学校 */
    @Excel(name = "毕业学校")
    private String exUniversity;

    /** 工作经历 */
    @Excel(name = "工作经历")
    private String exWork;

    public void setTeacherCode(String teacherCode) 
    {
        this.teacherCode = teacherCode;
    }

    public String getTeacherCode() 
    {
        return teacherCode;
    }
    public void setExUniversity(String exUniversity) 
    {
        this.exUniversity = exUniversity;
    }

    public String getExUniversity() 
    {
        return exUniversity;
    }
    public void setExWork(String exWork) 
    {
        this.exWork = exWork;
    }

    public String getExWork() 
    {
        return exWork;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("teacherCode", getTeacherCode())
            .append("exUniversity", getExUniversity())
            .append("exWork", getExWork())
            .toString();
    }
}
