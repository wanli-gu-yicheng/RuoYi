package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.EduKlass;

/**
 * 班级Service接口
 * 
 * @author ruoyi
 * @date 2024-03-31
 */
public interface IEduKlassService 
{
    /**
     * 查询班级
     * 
     * @param klassId 班级主键
     * @return 班级
     */
    public EduKlass selectEduKlassByKlassId(Long klassId);

    /**
     * 查询班级列表
     * 
     * @param eduKlass 班级
     * @return 班级集合
     */
    public List<EduKlass> selectEduKlassList(EduKlass eduKlass);

    /**
     * 新增班级
     * 
     * @param eduKlass 班级
     * @return 结果
     */
    public int insertEduKlass(EduKlass eduKlass);

    /**
     * 修改班级
     * 
     * @param eduKlass 班级
     * @return 结果
     */
    public int updateEduKlass(EduKlass eduKlass);

    /**
     * 批量删除班级
     * 
     * @param klassIds 需要删除的班级主键集合
     * @return 结果
     */
    public int deleteEduKlassByKlassIds(String klassIds);

    /**
     * 删除班级信息
     * 
     * @param klassId 班级主键
     * @return 结果
     */
    public int deleteEduKlassByKlassId(Long klassId);

    List<EduKlass> selectEduKlassAll();
}
