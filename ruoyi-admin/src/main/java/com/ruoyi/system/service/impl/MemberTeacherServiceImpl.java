package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.MemberTeacherMapper;
import com.ruoyi.system.domain.MemberTeacher;
import com.ruoyi.system.service.IMemberTeacherService;
import com.ruoyi.common.core.text.Convert;

/**
 * 教师Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Service
public class MemberTeacherServiceImpl implements IMemberTeacherService 
{
    @Autowired
    private MemberTeacherMapper memberTeacherMapper;

    /**
     * 查询教师
     * 
     * @param teacherId 教师主键
     * @return 教师
     */
    @Override
    public MemberTeacher selectMemberTeacherByTeacherId(Long teacherId)
    {
        return memberTeacherMapper.selectMemberTeacherByTeacherId(teacherId);
    }

    /**
     * 查询教师列表
     * 
     * @param memberTeacher 教师
     * @return 教师
     */
    @Override
    public List<MemberTeacher> selectMemberTeacherList(MemberTeacher memberTeacher)
    {
        return memberTeacherMapper.selectMemberTeacherList(memberTeacher);
    }

    /**
     * 新增教师
     * 
     * @param memberTeacher 教师
     * @return 结果
     */
    @Override
    public int insertMemberTeacher(MemberTeacher memberTeacher)
    {
        return memberTeacherMapper.insertMemberTeacher(memberTeacher);
    }

    /**
     * 修改教师
     * 
     * @param memberTeacher 教师
     * @return 结果
     */
    @Override
    public int updateMemberTeacher(MemberTeacher memberTeacher)
    {
        return memberTeacherMapper.updateMemberTeacher(memberTeacher);
    }

    /**
     * 批量删除教师
     * 
     * @param teacherIds 需要删除的教师主键
     * @return 结果
     */
    @Override
    public int deleteMemberTeacherByTeacherIds(String teacherIds)
    {
        return memberTeacherMapper.deleteMemberTeacherByTeacherIds(Convert.toStrArray(teacherIds));
    }

    /**
     * 删除教师信息
     * 
     * @param teacherId 教师主键
     * @return 结果
     */
    @Override
    public int deleteMemberTeacherByTeacherId(Long teacherId)
    {
        return memberTeacherMapper.deleteMemberTeacherByTeacherId(teacherId);
    }

    @Override
    public List<MemberTeacher> selectTeacherAll() {
        return memberTeacherMapper.selectMemberTeacherAll();
    }
}
