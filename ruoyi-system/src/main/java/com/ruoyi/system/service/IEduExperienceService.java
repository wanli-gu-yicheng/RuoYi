package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.EduExperience;

/**
 * 工作经历Service接口
 * 
 * @author ruoyi
 * @date 2024-03-17
 */
public interface IEduExperienceService 
{
    /**
     * 查询工作经历
     * 
     * @param teacherCode 工作经历主键
     * @return 工作经历
     */
    public EduExperience selectEduExperienceByTeacherCode(String teacherCode);

    /**
     * 查询工作经历列表
     * 
     * @param eduExperience 工作经历
     * @return 工作经历集合
     */
    public List<EduExperience> selectEduExperienceList(EduExperience eduExperience);

    /**
     * 新增工作经历
     * 
     * @param eduExperience 工作经历
     * @return 结果
     */
    public int insertEduExperience(EduExperience eduExperience);

    /**
     * 修改工作经历
     * 
     * @param eduExperience 工作经历
     * @return 结果
     */
    public int updateEduExperience(EduExperience eduExperience);

    /**
     * 批量删除工作经历
     * 
     * @param teacherCodes 需要删除的工作经历主键集合
     * @return 结果
     */
    public int deleteEduExperienceByTeacherCodes(String teacherCodes);

    /**
     * 删除工作经历信息
     * 
     * @param teacherCode 工作经历主键
     * @return 结果
     */
    public int deleteEduExperienceByTeacherCode(String teacherCode);
}
