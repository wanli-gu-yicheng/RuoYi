package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.MemberTeacher;

/**
 * 教师Service接口
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public interface IMemberTeacherService 
{
    /**
     * 查询教师
     * 
     * @param teacherId 教师主键
     * @return 教师
     */
    public MemberTeacher selectMemberTeacherByTeacherId(Long teacherId);

    /**
     * 查询教师列表
     * 
     * @param memberTeacher 教师
     * @return 教师集合
     */
    public List<MemberTeacher> selectMemberTeacherList(MemberTeacher memberTeacher);

    /**
     * 新增教师
     * 
     * @param memberTeacher 教师
     * @return 结果
     */
    public int insertMemberTeacher(MemberTeacher memberTeacher);

    /**
     * 修改教师
     * 
     * @param memberTeacher 教师
     * @return 结果
     */
    public int updateMemberTeacher(MemberTeacher memberTeacher);

    /**
     * 批量删除教师
     * 
     * @param teacherIds 需要删除的教师主键集合
     * @return 结果
     */
    public int deleteMemberTeacherByTeacherIds(String teacherIds);

    /**
     * 删除教师信息
     * 
     * @param teacherId 教师主键
     * @return 结果
     */
    public int deleteMemberTeacherByTeacherId(Long teacherId);


    List<MemberTeacher> selectTeacherAll();
}
