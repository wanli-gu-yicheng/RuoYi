package com.ruoyi.quartz.task;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiRobotSendRequest;
import com.dingtalk.api.response.OapiRobotSendResponse;
import com.ruoyi.system.domain.EduExam;
import com.ruoyi.system.service.IEduExamService;
import com.taobao.api.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ruoyi.common.utils.StringUtils;

import java.util.List;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{
    @Autowired
    private IEduExamService eduExamService;
    public void ryMultipleParams(String s, Boolean b, Long l, Double d, Integer i)
    {
        System.out.println(StringUtils.format("执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}", s, b, l, d, i));
    }

    public void ryParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void ryNoParams()
    {
        System.out.println("执行无参方法");
        List<EduExam> exams = eduExamService.selectEduExamList(new EduExam());

        StringBuilder sb = new StringBuilder("【提醒】\n");

        for (EduExam exam: exams) {
            if(exam.getScore() >=90){
                sb.append("Student: " + exam.getStudentId() + " Course: " + exam.getCourseId() + " Score: " + exam.getScore() + "\n");
            }
        }
        sendDingTalkMsg(sb.toString());
    }

    public static  void main(String[] args){
        ryParams(90);
    }

    private static void ryParams(int scoretest ) {
        System.out.println("考试成绩大于等于"+scoretest);

    }



    private static void sendDingTalkMsg(String content) {
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/robot/send?access_token=11974ccb704f9a762244a18121259eaf89199988c8cbca19beecba1d4a68be97");
        OapiRobotSendRequest request = new OapiRobotSendRequest();
        request.setMsgtype("text");
        OapiRobotSendRequest.Text text = new OapiRobotSendRequest.Text();
        text.setContent(content);
        request.setText(text);
        OapiRobotSendRequest.At at = new OapiRobotSendRequest.At();
        at.setIsAtAll(false);
        request.setAt(at);

        try {
            OapiRobotSendResponse response = client.execute(request);
        } catch (ApiException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
