package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.ArrayList;
import com.ruoyi.common.core.domain.Ztree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.EduReginMapper;
import com.ruoyi.system.domain.EduRegin;
import com.ruoyi.system.service.IEduReginService;
import com.ruoyi.common.core.text.Convert;

/**
 * 地区Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Service
public class EduReginServiceImpl implements IEduReginService 
{
    @Autowired
    private EduReginMapper eduReginMapper;

    /**
     * 查询地区
     * 
     * @param reginId 地区主键
     * @return 地区
     */
    @Override
    public EduRegin selectEduReginByReginId(Long reginId)
    {
        return eduReginMapper.selectEduReginByReginId(reginId);
    }

    /**
     * 查询地区列表
     * 
     * @param eduRegin 地区
     * @return 地区
     */
    @Override
    public List<EduRegin> selectEduReginList(EduRegin eduRegin)
    {
        return eduReginMapper.selectEduReginList(eduRegin);
    }

    /**
     * 新增地区
     * 
     * @param eduRegin 地区
     * @return 结果
     */
    @Override
    public int insertEduRegin(EduRegin eduRegin)
    {
        return eduReginMapper.insertEduRegin(eduRegin);
    }

    /**
     * 修改地区
     * 
     * @param eduRegin 地区
     * @return 结果
     */
    @Override
    public int updateEduRegin(EduRegin eduRegin)
    {
        return eduReginMapper.updateEduRegin(eduRegin);
    }

    /**
     * 批量删除地区
     * 
     * @param reginIds 需要删除的地区主键
     * @return 结果
     */
    @Override
    public int deleteEduReginByReginIds(String reginIds)
    {
        return eduReginMapper.deleteEduReginByReginIds(Convert.toStrArray(reginIds));
    }

    /**
     * 删除地区信息
     * 
     * @param reginId 地区主键
     * @return 结果
     */
    @Override
    public int deleteEduReginByReginId(Long reginId)
    {
        return eduReginMapper.deleteEduReginByReginId(reginId);
    }

    /**
     * 查询地区树列表
     * 
     * @return 所有地区信息
     */
    @Override
    public List<Ztree> selectEduReginTree()
    {
        List<EduRegin> eduReginList = eduReginMapper.selectEduReginList(new EduRegin());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (EduRegin eduRegin : eduReginList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(eduRegin.getReginId());
            ztree.setpId(eduRegin.getParentId());
            ztree.setName(eduRegin.getReginName());
            ztree.setTitle(eduRegin.getReginName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
