package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.TreeEntity;

/**
 * 地区对象 edu_regin
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public class EduRegin extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 地区ID */
    private Long reginId;

    /** 地区名 */
    @Excel(name = "地区名")
    private String reginName;

    /** 邮编 */
    @Excel(name = "邮编")
    private String zipCode;

    public void setReginId(Long reginId) 
    {
        this.reginId = reginId;
    }

    public Long getReginId() 
    {
        return reginId;
    }
    public void setReginName(String reginName) 
    {
        this.reginName = reginName;
    }

    public String getReginName() 
    {
        return reginName;
    }
    public void setZipCode(String zipCode) 
    {
        this.zipCode = zipCode;
    }

    public String getZipCode() 
    {
        return zipCode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("reginId", getReginId())
            .append("parentId", getParentId())
            .append("reginName", getReginName())
            .append("zipCode", getZipCode())
            .toString();
    }
}
