package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.EduRegin;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 地区Service接口
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public interface IEduReginService 
{
    /**
     * 查询地区
     * 
     * @param reginId 地区主键
     * @return 地区
     */
    public EduRegin selectEduReginByReginId(Long reginId);

    /**
     * 查询地区列表
     * 
     * @param eduRegin 地区
     * @return 地区集合
     */
    public List<EduRegin> selectEduReginList(EduRegin eduRegin);

    /**
     * 新增地区
     * 
     * @param eduRegin 地区
     * @return 结果
     */
    public int insertEduRegin(EduRegin eduRegin);

    /**
     * 修改地区
     * 
     * @param eduRegin 地区
     * @return 结果
     */
    public int updateEduRegin(EduRegin eduRegin);

    /**
     * 批量删除地区
     * 
     * @param reginIds 需要删除的地区主键集合
     * @return 结果
     */
    public int deleteEduReginByReginIds(String reginIds);

    /**
     * 删除地区信息
     * 
     * @param reginId 地区主键
     * @return 结果
     */
    public int deleteEduReginByReginId(Long reginId);

    /**
     * 查询地区树列表
     * 
     * @return 所有地区信息
     */
    public List<Ztree> selectEduReginTree();
}
