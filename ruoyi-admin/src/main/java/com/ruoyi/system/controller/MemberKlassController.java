package com.ruoyi.system.controller;

import java.lang.reflect.Member;
import java.util.List;

import com.ruoyi.system.domain.MemberTeacher;
import com.ruoyi.system.service.IMemberTeacherService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.MemberKlass;
import com.ruoyi.system.service.IMemberKlassService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 班级表Controller
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Controller
@RequestMapping("/system/klass")
public class MemberKlassController extends BaseController
{
    private String prefix = "system/klass";

    @Autowired
    private IMemberKlassService memberKlassService;

    @Autowired
    private IMemberTeacherService memberTeacherService;

    @RequiresPermissions("system:klass:view")
    @GetMapping()
    public String klass()
    {
        return prefix + "/klass";
    }

    /**
     * 查询班级表列表
     */
    @RequiresPermissions("system:klass:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(MemberKlass memberKlass)
    {
        startPage();
        List<MemberKlass> list = memberKlassService.selectMemberKlassList(memberKlass);
        return getDataTable(list);
    }

    /**
     * 导出班级表列表
     */
    @RequiresPermissions("system:klass:export")
    @Log(title = "班级表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(MemberKlass memberKlass)
    {
        List<MemberKlass> list = memberKlassService.selectMemberKlassList(memberKlass);
        ExcelUtil<MemberKlass> util = new ExcelUtil<MemberKlass>(MemberKlass.class);
        return util.exportExcel(list, "班级表数据");
    }

    /**
     * 新增班级表
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
        mmap.put("teachers", memberTeacherService.selectTeacherAll());
        return prefix + "/add";
    }

    /**
     * 新增保存班级表
     */
    @RequiresPermissions("system:klass:add")
    @Log(title = "班级表", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(MemberKlass memberKlass)
    {
        return toAjax(memberKlassService.insertMemberKlass(memberKlass));
    }

    /**
     * 修改班级表
     */
    @RequiresPermissions("system:klass:edit")
    @GetMapping("/edit/{klassId}")
    public String edit(@PathVariable("klassId") Long klassId, ModelMap mmap)
    {
        MemberKlass memberKlass = memberKlassService.selectMemberKlassByKlassId(klassId);
        mmap.put("memberKlass", memberKlass);
        List<MemberTeacher> teachers = memberTeacherService.selectTeacherAll();
        mmap.put("teachers", teachers);
        return prefix + "/edit";
    }

    /**
     * 修改保存班级表
     */
    @RequiresPermissions("system:klass:edit")
    @Log(title = "班级表", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(MemberKlass memberKlass)
    {
        return toAjax(memberKlassService.updateMemberKlass(memberKlass));
    }

    /**
     * 删除班级表
     */
    @RequiresPermissions("system:klass:remove")
    @Log(title = "班级表", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(memberKlassService.deleteMemberKlassByKlassIds(ids));
    }
}
