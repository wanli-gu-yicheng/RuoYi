package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.MemberKlass;

/**
 * 班级表Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public interface MemberKlassMapper 
{
    /**
     * 查询班级表
     * 
     * @param klassId 班级表主键
     * @return 班级表
     */
    public MemberKlass selectMemberKlassByKlassId(Long klassId);

    /**
     * 查询班级表列表
     * 
     * @param memberKlass 班级表
     * @return 班级表集合
     */
    public List<MemberKlass> selectMemberKlassList(MemberKlass memberKlass);

    /**
     * 新增班级表
     * 
     * @param memberKlass 班级表
     * @return 结果
     */
    public int insertMemberKlass(MemberKlass memberKlass);

    /**
     * 修改班级表
     * 
     * @param memberKlass 班级表
     * @return 结果
     */
    public int updateMemberKlass(MemberKlass memberKlass);

    /**
     * 删除班级表
     * 
     * @param klassId 班级表主键
     * @return 结果
     */
    public int deleteMemberKlassByKlassId(Long klassId);

    /**
     * 批量删除班级表
     * 
     * @param klassIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteMemberKlassByKlassIds(String[] klassIds);
}
